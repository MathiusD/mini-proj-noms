def extract_letter(patern_extract, first_name, last_name):
    out = {"first":"", "last":""}
    if len(first_name) > patern_extract['first']:
        out['first'] = first_name[patern_extract['first']].capitalize()
    else:
        out['first'] = first_name[len(first_name)-1].capitalize()
    if len(last_name) > patern_extract['last']:
        out['last'] = last_name[patern_extract['last']].capitalize()
    else:
        out['last'] = last_name[len(last_name)-1].capitalize()
    return out

def display_name(universe, letters):
    fictif = ""
    if letters['first'] in universe['first'].keys():
        fictif = universe['first'][letters['first']]
    else:
        fictif = universe['first']['default']
    fictif= fictif + " "
    if letters['last'] in universe['last'].keys():
        fictif = fictif + universe['last'][letters['last']]
    else:
        fictif = fictif +universe['last']['default']
    return fictif