import unittest
from gen_name.functions import extract_letter, display_name

class testCrypt(unittest.TestCase):
    
    def test_extract_letter(self):
        self.assertEqual({"first":"M","last":"Y"}, extract_letter({"first":0,"last":3}, "Mathieu", "Féry"))
        self.assertEqual({"first":"N","last":"S"}, extract_letter({"first":3,"last":99}, "Jean", "Tripes"))
        self.assertEqual({"first":"E","last":"I"}, extract_letter({"first":6,"last":0}, "Aucune", "Idée"))

    def test_display_name(self):
        universe = {
            "first":{
                "default":"IDK",
                "E":"Equal",
                "M":"Mabi",
                "N":"Nagy"
            },
            "last":{
                "default":"IDK",
                "I":"Interrogation",
                "S":"Soap",
                "Y":"Ya"
            }
        }
        self.assertEqual("Mabi Ya", display_name(universe, {"first":"M","last":"Y"}))
        self.assertEqual("Nagy Soap", display_name(universe, {"first":"N","last":"S"}))
        self.assertEqual("Equal Interrogation", display_name(universe, {"first":"E","last":"I"}))
        self.assertEqual("IDK IDK", display_name(universe, {"first":"_","last":"~"}))

    def test_croisés(self):
        universe = {
            "first":{
                "default":"IDK",
                "E":"Equal",
                "M":"Mabi",
                "N":"Nagy"
            },
            "last":{
                "default":"IDK",
                "I":"Interrogation",
                "S":"Soap",
                "Y":"Ya"
            }
        }
        self.assertEqual("Mabi Ya", display_name(universe, extract_letter({"first":0,"last":3}, "Mathieu", "Féry")))
        self.assertEqual("Nagy Soap", display_name(universe, extract_letter({"first":3,"last":99}, "Jean", "Tripes")))
        self.assertEqual("Equal Interrogation", display_name(universe, extract_letter({"first":6,"last":0}, "Aucune", "Idée")))