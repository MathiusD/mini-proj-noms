from tkinter.messagebox import *
from tkinter import *
from tkinter import ttk
from gen_name.functions import extract_letter, display_name
from data.names import universes

def button_callback():
    name = universe.get()
    letter = extract_letter(universes[name]['actions'], first_name.get("0.0", END).strip(), last_name.get("0.0", END).strip())
    fname = display_name(universes[name]['data'], letter)
    fictional_name.delete("0.0", END)
    fictional_name.insert("0.0", fname)

root = Tk()
root.title("Gen Names")

universe = ttk.Combobox(root, values=(list)(universes.keys()))
universe.current(0)
universe.grid(column=0, row=0)

first_name = Text(root, wrap='word')
first_name.insert(END, "")
first_name.grid(column=0, row=1)

last_name = Text(root, wrap='word')
last_name.insert(END, "")
last_name.grid(column=1, row=1)

root.rowconfigure(0, weight=1)
root.columnconfigure(1, weight=1)

button = Button(root, text="Process !", command=button_callback)
button.grid(row=2, column=0)

fictional_name = Text(root, wrap='word')
fictional_name.insert(END, "")
fictional_name.grid(column=0, row=3)

root.mainloop()