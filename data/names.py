universes = {
    "Star Wars" :
    {
        "actions":
        {
            "first":0,
            "last":2
        },
        "data":
        {
            "first":
            {
                "default": "Star",
                "A":"Darth",
                "B":"Padamé",
                "C":"Jar Jar",
                "D":"Obi-Wan",
                "E":"Emperor",
                "F":"Han",
                "G":"Greedo",
                "H":"Anakin",
                "I":"Count",
                "J":"Sarlacc",
                "K":"Jabba",
                "L":"Sergeant",
                "M":"Boba",
                "N":"Wicket W",
                "O":"Wampa",
                "P":"Senator",
                "Q":"Queen",
                "R":"Padawan",
                "S":"Imperial",
                "T":"General",
                "U":"Storm",
                "V":"R2",
                "W":"Luke",
                "X":"C3",
                "Y":"Mace",
                "Z":"Princess"
            },
            "last":
            {
                "default": "Wars",
                "A":"Bane",
                "B":"Sidious",
                "C":"Skywalker",
                "D":"Chewbacca",
                "E":"Fett",
                "F":"Trooper",
                "G":"PO",
                "H":"Speeder",
                "I":"Dooku",
                "J":"Pit",
                "K":"Vader",
                "L":"D2",
                "M":"Palpatine",
                "N":"Grievous",
                "O":"Bins",
                "P":"Soldier",
                "Q":"Kenobi",
                "R":"Solo",
                "S":"The Hutt",
                "T":"Maul",
                "U":"Windu",
                "V":"Queen",
                "W":"Leia",
                "X":"Monster",
                "Y":"Yoda",
                "Z":"Warrick"
            }
        }
    },
    "Game of Trones":
    {
        "actions":
        {
            "first":0,
            "last":0
        },
        "data":
        {
            "first":
            {
                "default": "Game",
                "A":"Bloody",
                "B":"Vulgar",
                "C":"Maddening",
                "D":"Ragged",
                "E":"Sordid",
                "F":"Mortal",
                "G":"Barbarous",
                "H":"Subbom",
                "I":"Cruel",
                "J":"Bitter",
                "K":"Jagged",
                "L":"Haunted",
                "M":"Sovereign",
                "N":"Crimson",
                "O":"Mighty",
                "P":"Ferral",
                "Q":"Imperial",
                "R":"Jagged",
                "S":"Lethal",
                "T":"Ferocious",
                "U":"Intrepid",
                "V":"Somber",
                "W":"Maddening",
                "X":"Ruthless",
                "Y":"Sacred",
                "Z":"Unforgiving"
            },
            "last":
            {
                "default": "Of Throne",
                "A":"Heartstriker",
                "B":"Nightfall",
                "C":"Vagabon",
                "D":"Oath",
                "E":"Judge",
                "F":"Breaker",
                "G":"Mayhem",
                "H":"Spirit",
                "I":"Commander",
                "J":"Marvel",
                "K":"Fire",
                "L":"Destroyer",
                "M":"Widowmaker",
                "N":"Vengance",
                "O":"Storm",
                "P":"Flame",
                "Q":"Blade",
                "R":"Chaos",
                "S":"Giant",
                "T":"Steel",
                "U":"Thunder",
                "V":"Hellhound",
                "W":"Butcher",
                "X":"Chaos",
                "Y":"Sorrow",
                "Z":"Jammer"               
            }
        }
    },
    "Wakfu":
    {
        "actions":
        {
            "first":2,
            "last":1
        },
        "data":
        {
            "first":
            {
                "default": "Wakfu",
                "A":"Adamaï",
                "B":"Amalia",
                "C":"Alibert",
                "D":"Evangéline",
                "E":"Ellely",
                "F":"Flopin",
                "G":"Goultard",
                "H":"Harebourg",
                "I":"Urhano",
                "J":"Armant",
                "K":"Dathura",
                "L":"Kya",
                "M":"Maude",
                "N":"Noximilien",
                "O":"Ombrage",
                "P":"Poo",
                "Q":"Oropo",
                "R":"Rubilax",
                "S":"Ruel",
                "T":"Toxine",
                "U":"Sylargh",
                "V":"Vampyro",
                "W":"Kayji",
                "X":"Nozgord",
                "Y":"Yugo",
                "Z":"Grougaloragran"
            },
            "last":
            {
                "default": "ToT",
                "A":"La Lenalde",
                "B":"L'Ezaréllé",
                "C":"L'Ancien",
                "D":"Steamer de la Première Heure",
                "E":"L'Infiltwé",
                "F":"Le Piktable",
                "G":"Maître des Sinistros",
                "H":"Fils du Dieu Iop",
                "I":"Survivant d'Agonie",
                "J":"Rechappé d'Externam",
                "K":"Mangé par la Shukrute",
                "L":"Le Suceur de Glace",
                "M":"Maître des éléments",
                "N":"Faconné par le monde",
                "O":"Dragon ancien",
                "P":"de Percedal",
                "Q":"Venant d'Oma",
                "R":"Maître Charcutier",
                "S":"Dresseur par passion",
                "T":"Brâkamrien dans l'âme",
                "U":"Histolique Anonyme",
                "V":"Fervant prêcheur",
                "W":"Rescapé d'Ogrest",
                "X":"Visiteur des profondeurs",
                "Y":"Amant de Dathura",
                "Z":"L'Eternel"               
            }
        }
    }
}